# Backend Test API
This is the API backend for geekseat test answer

## Table of Contents
* Getting Started
* Database Model
* Tech Stack
* Why Express.js and Sequelize

## Getting Started
To run the project for the first time, follow these steps:

1. Clone the repository:
    ```
    git clone https://gitlab.com/danztensai/geekseatapitest.git
    ```
2. Navigate to the project directory:

    ```
    cd geekseatapitest
    ```

3. Build and start the Docker containers:

    ```docker-compose up --build```

    or to run it in detached mode:

   ``` docker-compose up --build -d```

4. Access the Docker container for further setup:


    ```docker-compose exec toodolistapp bash```

    Inside the container, execute the following command to seed the database with example data:

    ``` ./start-seeding.sh ```

    This will populate the database with initial data for testing and development.

    After you run ``` start-seeding.sh ``` there will be 1 user with admin role, all user have default password `1234`

    ```  
     Username: 'userAdmin',
     password: '1234'
    ```




The API should now be running at http://localhost:3000/.

# Local Development
1. run ```npm install```
2. update ```config/config.json``` to match your database connection on local development
3. run ```npm run dev``` the hot reload will be up 

# Run Test
1. set NODE_ENV in ```.env``` into `test`
2. run ```npm run text``` or ```npx mocha test```


## Project Architecture 

* Middleware
    Middleware functions are used to perform tasks such as authentication, input validation, and request preprocessing before reaching the main controller. They act as intermediaries between the client's request and the controller, adding an extra layer of logic.

* Routes
    The routes module defines the URL routes that the application responds to. Each route is associated with a specific controller function that handles the request and provides a response. It helps in mapping HTTP requests to specific actions.

* Controller
    Controllers are responsible for handling incoming requests, processing the data, interacting with the database (if necessary), and sending back the appropriate response. They act as the core logic of your application.

* Config:
    The config module often contains configuration settings for your application, such as database connection details, environment variables, or other global settings. It helps in managing and centralizing configuration.

* Validation
    The validation module is used for input validation. It ensures that the data received from clients is valid and meets certain criteria before processing. It helps in preventing malicious or incorrect data from entering your system.

* Seeders
    Seeders are scripts that populate your database with initial or sample data. They are useful for setting up a development or testing environment with predefined data.

* Migration:
    Migration scripts are used to manage changes in your database schema over time. They help in versioning and applying database schema changes in a structured and repeatable manner.

* Models
    Models represent the data structures of your application and define how data is stored and retrieved from the database. They often correspond to database tables and provide an abstraction layer for database interactions.

This modular architecture helps in maintaining a clean and organized codebase, making it easier to develop, test, and scale your application. Each module has a specific responsibility, promoting separation of concerns and maintainability.

## Tech Stack
The project uses the following technologies and libraries:

* Node.js: A runtime environment for executing JavaScript on the server-side.

* Express.js: A minimal and flexible Node.js web application framework used for building robust APIs.

*  PostgreSQL: A powerful, open-source relational database management system.

* Docker: A containerization platform that allows you to package applications and their dependencies into containers for easy deployment.

* Sequelize: A promise-based Node.js ORM for PostgreSQL, which simplifies database operations and provides a clean API for database interactions.

* Joi: A schema description language and validator for JavaScript objects. It is used for validating API request payloads.

* Winston: A versatile logging library for Node.js applications, allowing you to log various information, including errors, HTTP requests, and custom events.

* Docker Compose: A tool for defining and running multi-container Docker applications. It simplifies container orchestration and configuration.
