// routes/task.js
const express = require('express');
const router = express.Router();
const taskController = require('../controllers/taskController');
const { verifyToken, isManager, isTaskOwner } = require('../middleware/jwtMiddleware');

// POST create a new task
router.post('/',verifyToken, taskController.createTask);

// GET get all tasks
router.get('/',verifyToken, taskController.getTasks);

// GET get a task by ID
router.get('/:taskId',verifyToken, taskController.getTaskById);

// PUT update a task by ID
router.put('/:taskId',verifyToken, isTaskOwner, taskController.updateTask);

// DELETE delete a task by ID
router.delete('/:taskId',verifyToken, isManager, taskController.deleteTask);

// PUT update task completion by task ID
router.put('/complete/:taskId', verifyToken, taskController.updateTaskCompletion);


module.exports = router;
