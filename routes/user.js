const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const { verifyToken, isAdmin } = require('../middleware/jwtMiddleware');

// POST create a user
router.post('/',verifyToken, isAdmin, userController.createUser);

// GET get all users (protected route)
router.get('/', verifyToken, userController.getAllUsers);

// GET get a user by ID (protected route)
router.get('/:id', verifyToken, userController.getUserById);

// PUT update a user by ID (protected route)
router.put('/:id', verifyToken, isAdmin, userController.updateUser);

// DELETE delete a user by ID (protected route)
router.delete('/:id', verifyToken, isAdmin, userController.deleteUser);

// PUT update User Role (protected route)
router.put('/:id/update-role', verifyToken, isAdmin, userController.updateUserRole);


module.exports = router;
