require('dotenv').config();
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser = require('body-parser');
const cors = require('cors');
const logger = require('./logger');
const authRoutes = require('./routes/auth');
const userRoutes = require('./routes/user');
const taskRoutes = require('./routes/task');



// Middleware
app.use(bodyParser.json());
app.use(cors());
// Logger middleware
app.use((req, res, next) => {
  const start = new Date();
  const requestDetails = {
    method: req.method,
    endpoint: req.originalUrl,
    headers: req.headers,
    params: req.params,
    body: req.body,
    query: req.query,
  };

  // Log the request details
  logger.http(`Incoming request: ${JSON.stringify(requestDetails)}`);

  res.on('finish', () => {
    const end = new Date();
    const responseTime = end - start;
    logger.http(`Response sent in ${responseTime}ms`);
  });

  next();
});
// Routes
app.use('/auth', authRoutes);
app.use('/users', userRoutes);
app.use('/tasks', taskRoutes);

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

module.exports = app;
