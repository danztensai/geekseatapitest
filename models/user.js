'use strict';
const { Model } = require('sequelize');
const bcrypt = require('bcrypt'); // Import the bcrypt library

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    // eslint-disable-next-line no-unused-vars
    static associate(models) {
      // define association here
      User.belongsToMany(models.Role, {
        through: 'UserRoles',
        foreignKey: 'UserId',
        otherKey: 'RoleId',
        as: 'roles', // Specify an alias to make the association clear
      });
    }

  }

  User.init(
    {
      UserID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      Username: DataTypes.STRING,
      Email: DataTypes.STRING,
      password: {
        type: DataTypes.STRING, // Use DataTypes.STRING instead of Sequelize.STRING
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: 'User',
    }
  );

  // Define a method for hashing passwords
  User.beforeCreate(async user => {
    const defaultPassword = '1234';
    if (user.password) {
      const saltRounds = 10; // You can adjust the number of salt rounds
      user.password = await bcrypt.hash(user.password, saltRounds);
    } else {
      const saltRounds = 10; // You can adjust the number of salt rounds
      user.password = await bcrypt.hash(defaultPassword, saltRounds);
    }
  });

  // Define a method for verifying passwords
  User.prototype.verifyPassword = async function (password) {
    return bcrypt.compare(password, this.password);
  };

  return User;
};
