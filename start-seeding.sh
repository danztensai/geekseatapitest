#!/bin/bash

# Run seed commands sequentially
npx sequelize db:seed --seed todolist-seed.js
# Exit with the status of the last command executed
exit $?
