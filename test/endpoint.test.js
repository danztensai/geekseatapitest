/* eslint-disable no-undef */
const { join } = require('path');
require('dotenv').config({ path: join(__dirname, '..', 'test.env') });
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../index'); // Import your Express app
const db = require('../models/index'); // Sequelize connection
chai.use(chaiHttp);
const expect = chai.expect;

describe('TodoList API', () => {
  before(async () => {
    await db.sequelize.sync({ force: true }); // Force sync to reset the database
    await db.Role.create({
        name: 'Admin',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      );
      await db.Role.create({
        name: 'User',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      );
      await db.Role.create({
        name: 'Manager',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      );
      // const adminRole = await db.Role.findOne({
      //   where: { name: 'Admin' },
      // }, ['id']);
      
      // const userRole = await db.Role.findOne({
      //   where: { name: 'User' },
      // }, ['id']);

      // const managerRole = await db.Role.findOne({
      //   where: { name: 'Manager' },
      // }, ['id']);

      await db.User.bulkCreate([
        {
          Username: 'userAdmin',
          Email: 'userAdmin@example.com',
          password: '1234',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          Username: 'user2',
          Email: 'user2@example.com',
          password: '1234',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          Username: 'user3',
          Email: 'user3@example.com',
          password: '1234',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          Username: 'user4',
          Email: 'user4@example.com',
          password: '1234',
          createdAt: new Date(),
          updatedAt: new Date(),
        }], { individualHooks: true });

        const rawInsertQuery = `
          INSERT INTO "UserRoles" ("UserId", "RoleId", "createdAt", "updatedAt")
          VALUES (?, ?, NOW(), NOW())
        `;
        const valuesToInsert = [1, 1]; // Replace userId and roleId with your actual values

        // Execute the raw SQL query
        db.sequelize.query(rawInsertQuery, {
          replacements: valuesToInsert,
          type: db.sequelize.QueryTypes.INSERT,
        })
          .then(() => {
            console.log('Raw SQL query executed successfully.');
          })
          .catch(error => {
            console.error('Error executing raw SQL query:', error);
          });
  
  });

  // After tests, close the database connection
  after(async () => {
    await db.sequelize.close();
  });

  describe('TodoList API', () => {
    describe('POST /auth/register', () => {
      it('should register a new user and return the user data', done => {
        chai
          .request(app)
          .post('/auth/register')
          .send({ username: 'newuserTest', email: 'newusertest@example.com', password: 'newpassword' })
          .end((err, res) => {
            expect(res).to.have.status(201);
            expect(res.body).to.have.property('Username', 'newuserTest');
            expect(res.body).to.have.property('Email', 'newusertest@example.com');
            done();
          });
      });

      it('should return an error for duplicate registration', done => {
        chai
          .request(app)
          .post('/auth/register')
          .send({ username: 'newuserTest', email: 'newusertest@example.com', password: 'newpassword' })
          .end((err, res) => {
            expect(res).to.have.status(400);
            done();
          });
      });
    });
    describe('POST /auth/login', () => {
      it('should authenticate a user and return a token', done => {
        // Create a test user in the database if needed
  
        chai
          .request(app)
          .post('/auth/login')
          .send({ username: 'newuserTest', password: 'newpassword' })
          .end((err, res) => {
            expect(res).to.have.status(200);
            expect(res.body).to.have.property('token');
            done();
          });
      });
  
      it('should return an error for invalid credentials', done => {
        chai
          .request(app)
          .post('/auth/login')
          .send({ username: 'invaliduser', password: 'invalidpassword' })
          .end((err, res) => {
            expect(res).to.have.status(401);
            done();
          });
      });
    });
  });

  describe('Task API Endpoints', () => {
    let authToken; // Store the authentication token here
  
    // Before running the tests, perform login and get the authentication token
    before(done => {
      chai
        .request(app)
        .post('/auth/login')
        .send({ username: 'userAdmin', password: '1234' })
        .end((err, res) => {
          authToken = res.body.token;
          done();
        });
    });
  
    // Test creating a new task
    it('should create a new task when POST /tasks is called', done => {
      chai
        .request(app)
        .post('/tasks')
        .set('Authorization', `Bearer ${authToken}`)
        .send({ name: 'New Task', description: 'Task Description' , userId:1})
        .end((err, res) => {
          expect(res).to.have.status(201);
          expect(res.body).to.have.property('name', 'New Task');
          expect(res.body).to.have.property('description', 'Task Description');
          done();
        });
    });
  
    // Test getting all tasks
    it('should retrieve a list of tasks when GET /tasks is called', done => {
      chai
        .request(app)
        .get('/tasks')
        .set('Authorization', `Bearer ${authToken}`)
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body).to.be.an('array');
          done();
        });
    });
  
    // Test updating a task
    it('should update a task when PUT /tasks/:taskId is called', done => {
      const taskToUpdate = {
        name: 'Updated Task',
        description: 'Updated Description',
        userId: 1
      };
  
      db.Task.findOne({ where: { name: 'New Task' } }).then(task => {
        const taskId = task.id;
        chai
          .request(app)
          .put(`/tasks/${taskId}`)
          .set('Authorization', `Bearer ${authToken}`)
          .send(taskToUpdate)
          .end((err, res) => {
            expect(res).to.have.status(200);
            expect(res.body).to.have.property('name', 'Updated Task');
            expect(res.body).to.have.property('description', 'Updated Description');
            done();
          });
      });
    });
  
    // Test deleting a task
    it('should delete a task when DELETE /tasks/:taskId is called', done => {
      db.Task.findOne({ where: { name: 'Updated Task' } }).then(task => {
        const taskId = task.id;
  
        chai
          .request(app)
          .delete(`/tasks/${taskId}`)
          .set('Authorization', `Bearer ${authToken}`)
          .end((err, res) => {
            expect(res).to.have.status(200);
            expect(res.body).to.have.property('message', 'Task deleted successfully');
            done();
          });
      });
    });
  
    // Add more test cases for other scenarios and endpoints as needed
  });
});
