const jwt = require('jsonwebtoken');
const secretKey = 'rollingGloryApi';
const { Task } = require('../models');
// Middleware to verify JWT token
exports.verifyToken = (req, res, next) => {
  const authorizationHeader = req.headers.authorization; // Get the token from the request header

  if (!authorizationHeader) {
    return res.status(401).json({ error: 'Unauthorized' });
  }

  // Check if the authorization header starts with "Bearer "
  if (!authorizationHeader.startsWith('Bearer ')) {
    return res.status(401).json({ error: 'Invalid token format' });
  }

  // Extract the token without "Bearer " prefix
  const token = authorizationHeader.substring(7);

  // Verify the token
  jwt.verify(token, secretKey, (err, decoded) => {
    if (err) {
      return res.status(401).json({ error: 'Token invalid' });
    }
    req.user = decoded; // Attach user information to the request object
    next();
  });
};

exports.isAdmin = (req, res, next) => {
  if (req.user && req.user.Roles && req.user.Roles.includes('Admin')) {
    return next(); // User is an Admin, allow access
  }
  res.status(403).json({ error: 'Access denied' });
};

exports.isManager = (req, res, next) => {
  if (req.user && req.user.Roles && req.user.Roles.includes('Manager')||req.user.Roles.includes('Admin')) {
    return next(); // User is an Admin, allow access
  }
  res.status(403).json({ error: 'Access denied' });
};

// middleware/ownershipMiddleware.js
exports.isTaskOwner = async (req, res, next) => {
  const { taskId } = req.params;
  const { UserId } = req.user;

  try {
    const task = await Task.findByPk(taskId);

    if (!task || task.userId !== UserId) {
      return res.status(403).json({ error: 'Permission denied' });
    }

    next();
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  }
};
