#!/bin/bash

# Run migrations
npm run migrate

# Start the Node.js application
npm start
