const Joi = require('joi');

// Validation schema for creating a new user
const createUserSchema = Joi.object({
  Username: Joi.string().required(),
  Email: Joi.string().email().required(),
  password: Joi.string().min(6).required(), // Adjust the minimum password length as needed
});

// Validation schema for updating a user
const updateUserSchema = Joi.object({
  Username: Joi.string(),
  Email: Joi.string().email(),
  password: Joi.string().min(6), // Optional: Password can be updated but not required
});

module.exports = {
  createUserSchema,
  updateUserSchema,
};
