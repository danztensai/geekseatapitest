const Joi = require('joi');

const taskSchema = Joi.object({
    description: Joi.string().required(),
    name: Joi.string().required(),
    userId: Joi.number().required(),
    completed: Joi.boolean(),
  });
  
  const updateTaskSchema = Joi.object({
    description: Joi.string().optional(),
    name: Joi.string().optional(),
    completedAt: Joi.date().optional(),
  });
  
  module.exports = { taskSchema, updateTaskSchema };
