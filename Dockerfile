# Use an official Node.js runtime as the base image
FROM node:14

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy package.json and package-lock.json to the container
COPY package*.json ./

# Install application dependencies
RUN npm install --production

# Copy the rest of your application code into the container
COPY . .

# Copy the start.sh script into the container
COPY start.sh .

# Make the script executable
RUN chmod +x start.sh

# Expose the port your application will run on
EXPOSE 3000

# Define the command to run your application
CMD ["./start.sh"]
