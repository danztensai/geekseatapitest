#!/bin/bash

# Run migrations
npx sequelize db:migrate

# Exit with the status of the last command executed
exit $?
