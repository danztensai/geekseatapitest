/* eslint-disable no-unused-vars */
module.exports = {
    up: async (queryInterface, Sequelize) => {
      
      await queryInterface.bulkInsert('Roles', [
        {
          name: 'Admin',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'User',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'Manager',
          createdAt: new Date(),
          updatedAt: new Date(),
        },

      ]);

       // Get the role IDs for 'Admin' and 'User'
       const adminRole = await queryInterface.rawSelect('Roles', {
        where: { name: 'Admin' },
      }, ['id']);
      
      const userRole = await queryInterface.rawSelect('Roles', {
        where: { name: 'User' },
      }, ['id']);

      const managerRole = await queryInterface.rawSelect('Roles', {
        where: { name: 'Manager' },
      }, ['id']);

      
      queryInterface.bulkInsert('Users', [
        {
          Username: 'userAdmin',
          Email: 'userAdmin@example.com',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          Username: 'user2',
          Email: 'user2@example.com',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          Username: 'user3',
          Email: 'user3@example.com',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          Username: 'user4',
          Email: 'user4@example.com',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          Username: 'user5',
          Email: 'user5@example.com',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        // Add more user entries as needed
      ]);

       // Associate specific users with roles in the UserRoles table
    await queryInterface.bulkInsert('UserRoles', [
      {
        UserId: 1, // User 1
        RoleId: adminRole, // Admin role
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        UserId: 2, // User 2
        RoleId: userRole, // User role
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        UserId: 3, 
        RoleId: managerRole,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        UserId: 4, 
        RoleId: userRole,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        UserId: 5, 
        RoleId: userRole,
        createdAt: new Date(),
        updatedAt: new Date(),
      },

    ]);
    
    },
  
    down: async (queryInterface, Sequelize) => {
      // Remove seeded data (if needed)
      await queryInterface.bulkDelete('Users', null, {});
      await queryInterface.bulkDelete('Roles', null, {});
      await queryInterface.bulkDelete('UserRoles', null, {});
    },
  };
  