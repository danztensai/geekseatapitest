const { User, Role } = require('../models');
const bcrypt = require('bcrypt');
const { createUserSchema, updateUserSchema } = require('../validations/userValidation');

// Create a new user
exports.createUser = async (req, res) => {
  try {
    // Validate request body using Joi schema
    const { error, value } = createUserSchema.validate(req.body);

    if (error) {
      return res.status(400).json({
        errors: [
          {
            status: '400',
            title: 'Bad Request',
            detail: error.details[0].message,
          },
        ],
      });
    }

    const { Username, Email, password } = value;

    // Check if the user already exists
    const existingUser = await User.findOne({ where: { Email } });
    if (existingUser) {
      return res.status(400).json({
        errors: [
          {
            status: '400',
            title: 'Bad Request',
            detail: 'User already exists',
          },
        ],
      });
    }

    // Hash the user's password
    const saltRounds = 10;
    const hashedPassword = await bcrypt.hash(password, saltRounds);

    // Create a new user
    const newUser = await User.create({
      Username,
      Email,
      password: hashedPassword,
    });

    res.status(201).json({ data: newUser });
  } catch (error) {
    console.error(error);
    res.status(500).json({
      errors: [
        {
          status: '500',
          title: 'Internal Server Error',
          detail: 'Server error',
        },
      ],
    });
  }
};

// Get all users with pagination and ordering
exports.getAllUsers = async (req, res) => {
  try {
    const { page = 1, limit = 10, sort = 'UserID', order = 'asc' } = req.query;

    const offset = (page - 1) * limit;

    const users = await User.findAndCountAll({
      limit: parseInt(limit),
      offset: parseInt(offset),
      order: [[sort, order]],
    });

    const totalPages = Math.ceil(users.count / limit);

    const response = {
      data: users.rows,
      meta: {
        totalItems: users.count,
        totalPages,
        currentPage: parseInt(page),
      },
    };

    res.json(response);
  } catch (error) {
    console.error(error);
    res.status(500).json({
      errors: [
        {
          status: '500',
          title: 'Internal Server Error',
          detail: 'Server error',
        },
      ],
    });
  }
};

// Get a single user by ID
exports.getUserById = async (req, res) => {
  const { id } = req.params;
  try {
    const user = await User.findByPk(id);
    if (!user) {
      return res.status(404).json({
        errors: [
          {
            status: '404',
            title: 'Not Found',
            detail: 'User not found',
          },
        ],
      });
    }
    res.json({ data: user });
  } catch (error) {
    console.error(error);
    res.status(500).json({
      errors: [
        {
          status: '500',
          title: 'Internal Server Error',
          detail: 'Server error',
        },
      ],
    });
  }
};

// Update a user by ID
exports.updateUser = async (req, res) => {
  const { id } = req.params;
  try {
    // Validate request body using Joi schema
    const { error, value } = updateUserSchema.validate(req.body);

    if (error) {
      return res.status(400).json({
        errors: [
          {
            status: '400',
            title: 'Bad Request',
            detail: error.details[0].message,
          },
        ],
      });
    }

    const user = await User.findByPk(id);
    if (!user) {
      return res.status(404).json({
        errors: [
          {
            status: '404',
            title: 'Not Found',
            detail: 'User not found',
          },
        ],
      });
    }

    // Update only the provided fields from the validated request body
    await user.update(value);

    res.json({ data: user });
  } catch (error) {
    console.error(error);
    res.status(500).json({
      errors: [
        {
          status: '500',
          title: 'Internal Server Error',
          detail: 'Server error',
        },
      ],
    });
  }
};

// Delete a user by ID
exports.deleteUser = async (req, res) => {
  const { id } = req.params;
  try {
    const user = await User.findByPk(id);
    if (!user) {
      return res.status(404).json({
        errors: [
          {
            status: '404',
            title: 'Not Found',
            detail: 'User not found',
          },
        ],
      });
    }

    await user.destroy();
    res.json({ message: 'User deleted successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({
      errors: [
        {
          status: '500',
          title: 'Internal Server Error',
          detail: 'Server error',
        },
      ],
    });
  }
};

// Update a user's role by ID (only for admins)
exports.updateUserRole = async (req, res) => {
  const { id } = req.params;
  const { newRole } = req.body; // Assuming you send the new role in the request body

  try {
    // Check if the requesting user is an admin
    if (req.user.Roles.includes('Admin')) {
      // Find the user by ID
      const user = await User.findByPk(id);

      if (!user) {
        return res.status(404).json({ error: 'User not found' });
      }

      // Find the role by name (e.g., 'Admin', 'User')
      const role = await Role.findOne({ where: { name: newRole } });

      if (!role) {
        return res.status(400).json({ error: 'Invalid role' });
      }

      // Update the user's role
      await user.setRoles([role]);

      res.json({ message: 'User role updated successfully' });
    } else {
      res.status(403).json({ error: 'Access denied' });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  }
};
