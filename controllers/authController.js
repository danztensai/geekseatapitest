const {User} = require('../models');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const secretKey = 'rollingGloryApi'; //
const Joi = require('joi'); // Import Joi for validation

// Joi validation schema for login request
const loginSchema = Joi.object({
  username: Joi.string().required(),
  password: Joi.string().required(),
});

// User login
exports.login = async (req, res) => {
  const { username, password } = req.body;

  try {
    // Validate the request body using Joi schema
    // eslint-disable-next-line no-unused-vars
    const { error, value } = loginSchema.validate({ username, password });

    if (error) {
      return res.status(400).json({ error: error.details[0].message });
    }

    // Find the user by username
    const user = await User.findOne({ where: { Username: username }, include: 'roles' });

    if (!user) {
      return res.status(401).json({ error: 'Authentication failed' });
    }

    // Compare the provided password with the hashed password in the database
    const isPasswordValid = await bcrypt.compare(password, user.password); // Use user.password

    if (!isPasswordValid) {
      return res.status(401).json({ error: 'Authentication failed' });
    }

    // Create a JWT token for the user
    const token = jwt.sign(
      {
        UserId: user.UserID,
        Username: user.Username,
        Roles: user.roles.map(data => data.name),
      },
      secretKey,
      {
        expiresIn: '1h', // Token expires in 1 hour (adjust as needed)
      }
    );

    res.json({ token });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  }
};


// Register a new user
exports.registerUser = async (req, res) => {
  try {
    const {username, email, password} = req.body;

    // Check if the user already exists
    const existingUser = await User.findOne({ where: { Email:email } });
    if (existingUser) {
      return res.status(400).json({error: 'User already exists'});
    }

    // Generate a secure hash for the default password "1234"
    // const passwordHash = await bcrypt.hash(password, 10);

    // Create a new user with the hashed default password
    const newUser = await User.create({
      Username: username,
      Email: email,
      password: password,
    });

    res.status(201).json(newUser);
  } catch (error) {
    console.error(error);
    res.status(500).json({error: 'Server error'});
  }
};
