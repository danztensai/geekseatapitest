// controllers/taskController.js
const { Task } = require('../models');
const { taskSchema, updateTaskSchema } = require('../validations/taskValidation');

// Create a new task
exports.createTask = async (req, res) => {
  try {
    const { error, value } = taskSchema.validate(req.body);

    if (error) {
      return res.status(400).json({ error: error.details[0].message });
    }

    let { description, name, userId } = value;
    if (!userId) {
      userId = req.user.UserId;
    }
    const task = await Task.create({ name, description, userId });
    res.status(201).json(task);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  }
};

// Retrieve a list of tasks
exports.getTasks = async (req, res) => {
  try {
    const tasks = await Task.findAll();
    res.json(tasks);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  }
};

// Retrieve a specific task by ID
exports.getTaskById = async (req, res) => {
  const { taskId } = req.params;
  try {
    const task = await Task.findByPk(taskId);
    if (!task) {
      return res.status(404).json({ error: 'Task not found' });
    }
    res.json(task);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  }
};

// Update a task by ID
exports.updateTask = async (req, res) => {
  const { taskId } = req.params;
  try {
    const { error, value } = taskSchema.validate(req.body);

    if (error) {
      return res.status(400).json({ error: error.details[0].message });
    }

    const { description, name, completed, userId } = value;
    const task = await Task.findByPk(taskId);
    if (!task) {
      return res.status(404).json({ error: 'Task not found' });
    }

    if (req.user.UserId !== task.userId && req.user.Roles.indexOf('Admin') === -1) {
      return res.status(403).json({ error: 'Access denied' });
    }

    task.description = description;
    task.completed = completed;
    task.name = name;
    task.userId = userId;
    await task.save();
    res.json(task);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  }
};

// Delete a task by ID
exports.deleteTask = async (req, res) => {
  const { taskId } = req.params;
  try {
    const task = await Task.findByPk(taskId);
    if (!task) {
      return res.status(404).json({ error: 'Task not found' });
    }
    await task.destroy();
    res.json({ message: 'Task deleted successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  }
};

// Update task completion by task ID (only the owner or admin can do this)
exports.updateTaskCompletion = async (req, res) => {
  const { taskId } = req.params;
  const { completedAt } = req.body;
  
  try {
    // eslint-disable-next-line no-unused-vars
    const { error, value } = updateTaskSchema.validate({ completedAt });

    if (error) {
      return res.status(400).json({ error: error.details[0].message });
    }

    const task = await Task.findByPk(taskId);

    if (!task) {
      return res.status(404).json({ error: 'Task not found' });
    }

    if (req.user.UserId !== task.userId && req.user.Roles.indexOf('Admin') === -1) {
      return res.status(403).json({ error: 'Access denied' });
    }

    task.completedAt = completedAt;
    await task.save();
    res.json(task);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  }
};

